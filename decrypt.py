import json, sys, os
if sys.version_info[0] < 3:
    raise Exception("You need Python 3.")
from django.conf import settings

from config import *

#Put this repo next to awx. Bear in mind you might have to disable a bunch of junk in awx to make this work properly,
#but it's the easiest way to get the code to be identical.
script_path =os.path.dirname(os.path.realpath(__file__))
sys.path.insert(0,script_path+'/../awx')

from awx.main.utils import encryption
settings.configure(SECRET_KEY=vault_key)

#Spoof us an object
class Setting: pass
data = Setting()

#pg_dump likes to put newlines. Let's not.
inputs = inputs.replace('\n','')

#the decrypt_field function uses the SECRET_KEY, then the primary key, then the string itself, to do AES128 HMAC encryption.
#You need all three items to get the data.
data.ssh_key_data = json.loads(inputs)['ssh_key_data']
data.pk = id

decrypted = encryption.decrypt_field(data,'ssh_key_data')
print(decrypted)




